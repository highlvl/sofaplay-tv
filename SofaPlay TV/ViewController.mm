//
//  ViewController.m
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 04.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//


#import "ViewController.h"
#import "MediaPlayer.h"
#import "UPnPMediaRenderer.h"
#import <AVKit/AVKit.h>

typedef enum {
    RendererStateWaitingForConnection,
    RendererStateConnected
} RendererState;

static NSString *isPressedKeyPath = @"isPressed";

@interface ViewController () <UPnPMediaRendererDelegate, VLCMediaPlayerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic) BOOL connectionEstablished;
@property (nonatomic,assign) RendererState state;

@property(nonatomic, assign) SystemSoundID audioEffect;

@property (nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) CGPoint startLocation;
@property (nonatomic, assign) BOOL panRecognizerAllowed;
@property (nonatomic, readonly) NSTimer *timer;

@property (nonatomic, assign) BOOL open;
@property (nonatomic, assign) BOOL playerIsMuted;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.connectionEstablished = NO;
    self.panRecognizerAllowed = YES;
    self.playerIsMuted = NO;

    [[UPnPMediaRenderer sharedInstance] setDelegate:self];

    [MediaPlayer sharedInstance].delegate = self;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePlayPauseButtonPress:)];
    tapGestureRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypePlayPause] ];
    [self.view addGestureRecognizer:tapGestureRecognizer];
//
//    PLT_Service* avt = [self.renderer findServiceByType:@"urn:schemas-upnp-org:service:AVTransport:1"];
//    PLT_StateVariable* var;
//    var = avt->FindStateVariable("TransportState");
//    NSLog(@"%@", [NSString stringWithCString: var->GetValue() encoding:NSUTF8StringEncoding]  );

    [self setupView];
    
    NSString *path = [[NSBundle mainBundle] pathForResource: @"blip1" ofType: @"wav.m4a"];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath : path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &_audioEffect);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    _open = NO;
    
    _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction)];
    _panRecognizer.delegate = self;
    [self.view addGestureRecognizer:self.panRecognizer];
    
    self.topBarTopConstraint.constant = - self.topBarView.frame.size.height;
    [self.panRecognizer.view becomeFirstResponder];
    
    [self.licenseInfoButton addObserver:self forKeyPath: isPressedKeyPath options:NSKeyValueObservingOptionNew context:nil];
    
    [[UPnPMediaRenderer sharedInstance] startMediaRenderer];
    
    _timer = [NSTimer timerWithTimeInterval:30 target:self selector:@selector(restartUPnP) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self hideTopBarAnimated:YES];
}

- (void)licensesButtonAction {
    //NSLog(@"Licenses Button Action");
}

- (void)restartUPnP {
    NSLog(@"restart upnp");
    [[UPnPMediaRenderer sharedInstance] stopMediaRenderer];
    [[UPnPMediaRenderer sharedInstance] startMediaRenderer];
}

- (void)setPanRecognizerAllowed:(BOOL)panRecognizerAllowed {
    _panRecognizerAllowed = panRecognizerAllowed;
    [self.panRecognizer setEnabled:panRecognizerAllowed];
}

- (void)showTopBarAnimated:(BOOL)animated {
    CGFloat openConstant = 0.0;
    if (animated) {
        [UIView animateWithDuration:0.2 animations:^{
            self.topBarTopConstraint.constant = openConstant;
        } completion:^(BOOL finished) {
            self.open = YES;
        }];
    } else {
        self.topBarTopConstraint.constant = openConstant;
        self.open = YES;
    }
}

- (void)hideTopBarAnimated:(BOOL)animated {
    CGFloat topBarHeight = self.topBarView.frame.size.height;
    CGFloat closedConstant = - topBarHeight;
    if (animated) {
        [UIView animateWithDuration:0.2 animations:^{
            self.topBarTopConstraint.constant = closedConstant;
            self.arrowImageView.transform = CGAffineTransformMakeScale(1, 1); //Unflipped
        }];
    } else {
        self.topBarTopConstraint.constant = closedConstant;
        self.arrowImageView.transform = CGAffineTransformMakeScale(1, 1); //Unflipped
    }
    self.open = NO;
}

- (void)panAction {
    
    if (self.panRecognizerAllowed == false) {
        return;
    }
    
    CGFloat changed = 0.0;
    
    if (self.panRecognizer.state == UIGestureRecognizerStateBegan) {
        self.startLocation = [self.panRecognizer locationInView:self.view];
    }
    
    if (self.panRecognizer.state != UIGestureRecognizerStateBegan) {
        CGPoint stopLocation = [self.panRecognizer locationInView:self.view];
        changed = stopLocation.y - self.startLocation.y;
    }
    
    //NSLog(@"Did pan: %f", changed);
    
    CGFloat topBarHeight = self.topBarView.frame.size.height;
    
    CGFloat newTopConstraintConstant = 0.0;
    if (self.open) {
        CGFloat changeLimit = MIN(0.0, changed);
        changeLimit = MAX(-topBarHeight, changeLimit);
        newTopConstraintConstant = changeLimit;
    } else {
        CGFloat changeLimit = MAX(0.0, changed);
        changeLimit = MIN(topBarHeight, changeLimit);
        newTopConstraintConstant = (-topBarHeight) + changeLimit;
    }
    
    if (newTopConstraintConstant < 0.0) {
        self.licenseInfoButton.enabled = NO;
        self.licenseInfoButton.userInteractionEnabled = NO;
    }

    self.topBarTopConstraint.constant = newTopConstraintConstant;
    
    if (self.panRecognizer.state == UIGestureRecognizerStateEnded) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            CGFloat threshold = 20.0;
            
            if (newTopConstraintConstant >= 0.0 - threshold) {
                self.open = YES;
                self.licenseInfoButton.enabled = YES;
                self.licenseInfoButton.userInteractionEnabled = YES;
                self.licenseInfoButton.shouldFocus = YES;
                
                [UIView animateWithDuration:0.2 animations:^{
                    self.topBarTopConstraint.constant = 0.0;
                    //self.arrowImageView.alpha = 0.0;
                    self.arrowImageView.transform = CGAffineTransformMakeScale(1, -1); //Flipped

                }];
            } else {
                self.open = NO;
                self.licenseInfoButton.enabled = NO;
                self.licenseInfoButton.userInteractionEnabled = NO;
                self.licenseInfoButton.shouldFocus = NO;
                
                [UIView animateWithDuration:0.2 animations:^{
                    self.topBarTopConstraint.constant = - topBarHeight;
                    //self.arrowImageView.alpha = 1.0;
                    self.arrowImageView.transform = CGAffineTransformMakeScale(1, 1); //Unflipped
                }];
            }
            
            [self setNeedsFocusUpdate];
            [self updateFocusIfNeeded];
        }];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    //NSLog(@"should begin");
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    //NSLog(@"should receive touch");
    return YES;
}

- (void)dealloc {
    [self.licenseInfoButton removeObserver:self forKeyPath: isPressedKeyPath];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupView {
    _state = RendererStateWaitingForConnection;
    
    self.topBarView.layer.masksToBounds = NO;
    self.topBarView.layer.shadowOffset = CGSizeMake(-5, 0);
    self.topBarView.layer.shadowRadius = 5;
    self.topBarView.layer.shadowOpacity = 0.5;
    
    self.licenseInfoButton.shouldFocus = NO;
    self.licenseInfoButton.enabled = NO;
    self.licenseInfoButton.userInteractionEnabled = NO;
    [self.licenseInfoButton addTarget:self action:@selector(licensesButtonAction) forControlEvents:UIControlEventPrimaryActionTriggered];
    
    [self.checkBoxView setBackgroundColor:[UIColor clearColor]];
    [self.checkBoxView setLineWidth:4.0];
    [self.checkBoxView setTintColor:[UIColor clearColor]];
    [self.checkBoxView setOnCheckColor:[UIColor whiteColor]];
    [self.checkBoxView setOnFillColor:[UIColor clearColor]];
    [self.checkBoxView setOnTintColor:[UIColor whiteColor]];
    [self.checkBoxView setOnAnimationType:BEMAnimationTypeStroke];
    
    NSString *waitingTitle = NSLocalizedString(@"renderer_waiting_title", nil);
    NSString *waitingBody = NSLocalizedString(@"renderer_waiting_body", nil);
    self.titleLabel.text = waitingTitle;
    self.bodyLabel.text = waitingBody;
    
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator startAnimating];
}

- (void)setState:(RendererState)state {
    
    if (_state != state) {
        switch (state) {
            case RendererStateWaitingForConnection:
                [self transitionToStateWaitingForConnection];
                break;
            case RendererStateConnected:
                [self transitionToStateConnected];
                break;
            default:
                break;
        }
    }
    
    _state = state;
}

- (void)transitionToStateConnected {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

        [self.activityIndicator stopAnimating];
        
        NSString *connectedTitle = NSLocalizedString(@"connected_title", nil);
        NSString *connectedBody = NSLocalizedString(@"connected_body", nil);
        
        [UIView transitionWithView:self.view
                          duration:0.25
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.titleLabel.text = connectedTitle;
                            self.bodyLabel.text = connectedBody;
        } completion:nil];
        [self.checkBoxView setOn:YES animated:YES];
        AudioServicesPlaySystemSound(self.audioEffect);
    }];
}

- (void)transitionToStateWaitingForConnection {
    
    [self.checkBoxView setOn:NO animated:NO];
    [self.activityIndicator startAnimating];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSString *waitingTitle = NSLocalizedString(@"renderer_waiting_title", nil);
        NSString *waitingBody = NSLocalizedString(@"renderer_waiting_body", nil);
        self.titleLabel.text = waitingTitle;
        self.bodyLabel.text = waitingBody;
    }];
}

- (void)handleEnteredBackground {
    self.connectionEstablished = NO;
    self.state = RendererStateWaitingForConnection;
}

- (void)mediaPlayerTimeChanged:(NSNotification *)aNotification
{
    
    VLCMediaPlayer *player = [aNotification object];
    
    VLCTime *currentTime = player.time;
    VLCTime* durationTime = [player.media length];
    
    if(currentTime && durationTime){
        int position = [currentTime intValue];
        int duration = [durationTime intValue];
        [[UPnPMediaRenderer sharedInstance] setPositionVariable:position];
        [[UPnPMediaRenderer sharedInstance] setDurationVariable:duration];
    }
}

- (void)mediaPlayerStateChanged:(NSNotification *)aNotification
{
    VLCMediaPlayer *player = [aNotification object];
    
    if(player.state == VLCMediaPlayerStatePlaying || player.state == VLCMediaPlayerStateBuffering){
        //NSLog(@"Playing");
        [[UPnPMediaRenderer sharedInstance] setTransportStateToString:@"PLAYING"];
    } else if(player.state == VLCMediaPlayerStatePaused){
        //NSLog(@"Paused");
        [[UPnPMediaRenderer sharedInstance] setTransportStateToString:@"PAUSED_PLAYBACK"];
    } else if(player.state == VLCMediaPlayerStateEnded){
        //NSLog(@"Ended");
        [[UPnPMediaRenderer sharedInstance] setTransportStateToString:@"STOPPED"];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    } else if(player.state == VLCMediaPlayerStateStopped){
        //NSLog(@"Stopped");
        [[UPnPMediaRenderer sharedInstance] setTransportStateToString:@"STOPPED"];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    
}


- (void)handlePlayPauseButtonPress:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        if([MediaPlayer sharedInstance].isPlaying){
            [[MediaPlayer sharedInstance] pause];
        }else{
            [[MediaPlayer sharedInstance] play ];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if (object == self.licenseInfoButton && [keyPath isEqualToString: isPressedKeyPath]) {
        //NSLog(@"button state: %d", self.licenseInfoButton.isPressed);
        
        self.panRecognizerAllowed = self.licenseInfoButton.isPressed == false;
    }
}

#pragma mark - UPnPMediaRendererDelegate

- (void)didReceiveConnectionEstablished {
    if(!self.connectionEstablished){
        self.connectionEstablished = YES;
        NSLog(@"YES CONNECTION ESTABLISHED");
        self.state = RendererStateConnected;
        
        NSLog(@"invalidate Timer");
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)didReceiveSetMute:(BOOL)mute {
    [[MediaPlayer sharedInstance].audio setMuted:mute];
    self.playerIsMuted = mute;
    [[UPnPMediaRenderer sharedInstance] setMute:mute];
    //NSLog(@"YES CONNECTION ESTABLISHED");
}

- (void)didReceivePlay {
    [[MediaPlayer sharedInstance] play];
    
    //NSLog(@"PLAY");
}

- (void)didReceivePause {
    [[MediaPlayer sharedInstance] pause];
    
    //NSLog(@"PAUSE");
}

- (void)didReceiveStop {
    [[MediaPlayer sharedInstance] stop];
    //NSLog(@"STOP");
}

- (void)didReceiveSetAVTransportUri:(NSURL *)url {
    
    [MediaPlayer sharedInstance].drawable = [[UIApplication sharedApplication] keyWindow];
    
    VLCMedia *media = [VLCMedia mediaWithURL:url];

    [MediaPlayer sharedInstance].media = media;
    
    VLCTime* durationTime = [[MediaPlayer sharedInstance].media length];
    if(durationTime){
        [[UPnPMediaRenderer sharedInstance] setDurationVariable:[durationTime intValue]];
    }
    
    //NSLog(@"SET AV");
}

- (void)didReceiveSeek:(int)milliseconds {
    
    VLCTime* time = [VLCTime timeWithInt:milliseconds];
    [[MediaPlayer sharedInstance] setTime:time];
    
    //NSLog(@"Target: %d", milliseconds);
}

@end
