//
//  MediaPlayer.h
//  SofaPlay TV
//
//  Created by Gerald Voit on 17.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <TVVLCKit/TVVLCKit.h>

@interface MediaPlayer : VLCMediaPlayer

+ (instancetype)sharedInstance;

@end
