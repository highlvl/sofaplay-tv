//
//  ViewController.h
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 04.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TVVLCKit/TVVLCKit.h>
#import "BEMCheckBox.h"
#import "CustomButton.h"

@interface ViewController : UIViewController<VLCMediaPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarTopConstraint;
@property (weak, nonatomic) IBOutlet CustomButton *licenseInfoButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBoxView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

- (void)mediaPlayerTimeChanged:(NSNotification *)aNotification;
- (void)mediaPlayerStateChanged:(NSNotification *)aNotification;
- (void)handlePlayPauseButtonPress:(UITapGestureRecognizer *)sender;

@end

