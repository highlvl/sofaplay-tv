//
//  UPnPMediaRenderer.h
//  SofaPlay TV
//
//  Created by Gerald Voit on 12.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UPnPMediaRendererDelegate <NSObject>

- (void)didReceiveConnectionEstablished;
- (void)didReceiveSetMute:(BOOL)mute;
- (void)didReceivePlay;
- (void)didReceivePause;
- (void)didReceiveStop;
- (void)didReceiveSetAVTransportUri:(NSURL *)url;
- (void)didReceiveSeek:(int)milliseconds;

@end

@interface UPnPMediaRenderer : NSObject

@property (nonatomic,weak) id<UPnPMediaRendererDelegate> delegate;

+ (instancetype)sharedInstance;

- (void)startMediaRenderer;
- (void)stopMediaRenderer;
- (void)shutdownRenderer;

- (void)removeRenderer;
- (void)setRendererIfNeeded;

- (void)setMute: (BOOL)mute;
- (void)setTransportStateToString: (NSString*)string;
- (void)setPositionVariable:(int)positionInMilliseconds;
- (void)setDurationVariable:(int)durationInMilliseconds;
- (NSString *)timeFormatted:(int)totalMiliseconds;
- (int)timeInMilisecondsOfTimeString:(NSString *)timeString;

@end
