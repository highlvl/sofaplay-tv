//
//  AppDelegate.h
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 04.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

