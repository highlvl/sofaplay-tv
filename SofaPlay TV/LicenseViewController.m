//
//  LicenseViewController.m
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 13.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import "LicenseViewController.h"

@interface LicenseViewController ()

@end

@implementation LicenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_txtView setUserInteractionEnabled:YES];
    [_txtView setSelectable:YES];
    [_txtView.panGestureRecognizer setAllowedTouchTypes:[NSArray arrayWithObjects:[NSNumber numberWithInteger:UITouchTypeIndirect], nil]];

    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"license" ofType:@"txt"];
    NSString* licenseText = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    _txtView.text  = licenseText;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
