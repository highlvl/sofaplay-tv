//
//  main.m
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 04.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
