//
//  MediaPlayer.m
//  SofaPlay TV
//
//  Created by Gerald Voit on 17.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import "MediaPlayer.h"

@implementation MediaPlayer

+ (instancetype)sharedInstance {
    
    static MediaPlayer *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MediaPlayer alloc] init];
    });
    return sharedInstance;
}

@end
