//
//  Platinum.m
//  SofaPlay TV
//
//  Created by Gerald Voit on 12.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import "UPnPMediaRenderer.h"

#ifndef UIONLY
#import "Neptune/Neptune.h"
#import <Platinum/PltUPnPObject.h>
#import <Platinum/PltMediaServerObject.h>
#import <Platinum/PltService.h>
#endif

#ifndef UIONLY
@interface UPnPMediaRenderer () <PLT_MediaRendererDelegateObject>

@property (nonatomic) PLT_UPnPObject* upnp;
@property (nonatomic) PLT_MediaRendererObject* renderer;

@end
#endif

@implementation UPnPMediaRenderer

+ (instancetype)sharedInstance {
    
    static UPnPMediaRenderer *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UPnPMediaRenderer alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
#ifndef UIONLY
        NSLog(@"MediaRenderer init");
        self.upnp = [[PLT_UPnPObject alloc] init];
#endif
    }
    return self;
}

- (void)setRendererIfNeeded {
#ifndef UIONLY
    if (self.renderer == nil) {
        NSLog(@"set new MediaRenderer");
        self.renderer = [[PLT_MediaRendererObject alloc] init];
        self.renderer.delegate = self;
        [self.upnp addDevice:self.renderer];
    }
#endif
}

- (void)removeRenderer {
    NSLog(@"remove current MediaRenderer");
#ifndef UIONLY
    if (self.upnp && self.renderer) {
        [self.upnp removeDevice:self.renderer];
        self.renderer.delegate = nil;
        self.renderer = nil;
    }
#endif
}

- (void)startMediaRenderer {
#ifndef UIONLY
    NSLog(@"start upnp");
    [self setRendererIfNeeded];
    [self.upnp start];
#endif
}

- (void)stopMediaRenderer {
#ifndef UIONLY
    NSLog(@"stop upnp");
    [self.upnp stop];
#endif
}

- (void)shutdownRenderer {
#ifndef UIONLY
    NSLog(@"shutdown upnp");
    [self removeRenderer];
    [self.upnp stop];
#endif
}

#pragma MARK - Helper

- (void)setTransportStateToString: (NSString*)string {
#ifndef UIONLY
    if (self.upnp.isRunning) {
        PLT_Service* avt = [self.renderer findServiceByType:@"urn:schemas-upnp-org:service:AVTransport:1"];
        avt->SetStateVariable("TransportState", [string cStringUsingEncoding:NSUTF8StringEncoding]);
    }
#endif
}

- (void)setMute: (BOOL)mute {
#ifndef UIONLY
    if (self.upnp.isRunning) {
        PLT_Service* avt = [self.renderer findServiceByType:@"urn:schemas-upnp-org:service:RenderingControl:1"];
        if(mute){
            avt->SetStateVariable("Mute", "1");
        }else {
            avt->SetStateVariable("Mute", "0");
        }
    }
#endif
}

- (void)setPositionVariable:(int)positionInMilliseconds {
#ifndef UIONLY
    if (self.upnp.isRunning) {
        PLT_Service* avt = [self.renderer findServiceByType:@"urn:schemas-upnp-org:service:AVTransport:1"];
        NSString *positionString = [self timeFormatted:positionInMilliseconds];
        avt->SetStateVariable("RelativeTimePosition", [positionString cStringUsingEncoding:NSUTF8StringEncoding]);
    }
#endif
}

- (void)setDurationVariable:(int)durationInMilliseconds {
#ifndef UIONLY
    if (self.upnp.isRunning) {
        PLT_Service* avt = [self.renderer findServiceByType:@"urn:schemas-upnp-org:service:AVTransport:1"];
        NSString *durationString = [self timeFormatted:durationInMilliseconds];
        avt->SetStateVariable("CurrentTrackDuration", [durationString cStringUsingEncoding:NSUTF8StringEncoding]);
    }
#endif
}

- (NSString *)timeFormatted:(int)totalMiliseconds {
    
    int totalSeconds = totalMiliseconds / 1000;
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (int)timeInMilisecondsOfTimeString:(NSString *)timeString {
    
    if(timeString == nil || [timeString isEqualToString:@""]) {
        return 0;
    }
    
    NSArray *elements = [timeString componentsSeparatedByString:@":"];
    
    int hours, minutes, seconds;
    int totalSeconds = 0;
    
    int elementCount = (int)[elements count];
    
    if(elementCount == 3) {
        hours = [[elements objectAtIndex:0] doubleValue];
        minutes = [[elements objectAtIndex:1] doubleValue];
        seconds = [[elements objectAtIndex:2] doubleValue];
        
        totalSeconds = hours * 60 * 60 + minutes * 60 + seconds;
    } else if(elementCount == 2) {
        minutes = [[elements objectAtIndex:0] doubleValue];
        seconds = [[elements objectAtIndex:1] doubleValue];
        
        totalSeconds = minutes * 60 + seconds;
    } else {
        seconds = [[elements objectAtIndex:0] doubleValue];
        
        totalSeconds = seconds;
    }
    
    
    return  totalSeconds*1000;
}

#pragma MARK - PLT_MediaRendererDelegateObject

#ifndef UIONLY
- (void)onConnectionEstablished:(PLT_MediaRendererGeneralCapsule *)info {
    
    if ([self.delegate respondsToSelector:@selector(didReceiveConnectionEstablished)]) {
        [self.delegate didReceiveConnectionEstablished];
    }
}

- (NPT_Result)onGetCurrentConnectionInfo:(PLT_MediaRendererGeneralCapsule *)info {
    
    //NSLog(@"INFO");
    
    return NPT_SUCCESS;
}

- (NPT_Result)onSetMute:(PLT_MediaRendererSetMuteCapsule *)info {
    //NSLog(info.desiredMute ? @"Mute: Yes" : @"Mute: No");
    
    if ([self.delegate respondsToSelector:@selector(didReceiveSetMute:)]) {
        [self.delegate didReceiveSetMute:info.desiredMute];
    }
    
    return NPT_SUCCESS;
}

- (NPT_Result)onPlay:(PLT_MediaRendererGeneralCapsule *)info {
    
    if ([self.delegate respondsToSelector:@selector(didReceivePlay)]) {
        [self.delegate didReceivePlay];
    }
    [self setTransportStateToString:@"PLAYING"];
    
    return NPT_SUCCESS;
}

- (NPT_Result)onSetPlayMode:(PLT_MediaRendererGeneralCapsule *)info {
    
    //NSLog(@"PlayMode");
    
    return NPT_SUCCESS;
}

- (NPT_Result)onPause:(PLT_MediaRendererGeneralCapsule *)info {
    
    if ([self.delegate respondsToSelector:@selector(didReceivePause)]) {
        [self.delegate didReceivePause];
    }
    [self setTransportStateToString:@"PAUSED_PLAYBACK"];
    
    return NPT_SUCCESS;
}

- (NPT_Result)onStop:(PLT_MediaRendererGeneralCapsule *)info {
    
    if ([self.delegate respondsToSelector:@selector(didReceiveStop)]) {
        [self.delegate didReceiveStop];
    }
    [self setTransportStateToString:@"STOPPED"];
    [self setPositionVariable:0];
    [self setDurationVariable:0];
    
    return NPT_SUCCESS;
}

- (NPT_Result)onSetAVTransportUri:(PLT_MediaRendererSetAVTransportUriCapsule *)info{
    
    //NSLog(@"URL: %@", info.uri);
    
    NSURL *url = [NSURL URLWithString:info.uri];
    
    if ([self.delegate respondsToSelector:@selector(didReceiveSetAVTransportUri:)]) {
        [self.delegate didReceiveSetAVTransportUri:url];
    }
    
    return NPT_SUCCESS;
}

- (NPT_Result)onSeek:(PLT_MediaRendererSeekCapsule *)info{
    
    int seekPosition = [self timeInMilisecondsOfTimeString:info.target];
    
    if ([self.delegate respondsToSelector:@selector(didReceiveSeek:)]) {
        [self.delegate didReceiveSeek:seekPosition];
    }
    
    return NPT_SUCCESS;
}
#endif

@end
