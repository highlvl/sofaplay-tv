//
//  CustomButton.m
//  SofaPlay TV
//
//  Created by Gerald Voit on 11.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (void)pressesBegan:(NSSet<UIPress *> *)presses withEvent:(UIPressesEvent *)event {
    [super pressesBegan:presses withEvent:event];
    self.isPressed = YES;
}

- (void)pressesEnded:(NSSet<UIPress *> *)presses withEvent:(UIPressesEvent *)event {
    [super pressesEnded:presses withEvent:event];
    self.isPressed = NO;
}

- (void)pressesCancelled:(NSSet<UIPress *> *)presses withEvent:(UIPressesEvent *)event {
    [super pressesCancelled:presses withEvent:event];
    self.isPressed = NO;
}

- (BOOL)canBecomeFocused {
    return _shouldFocus;
}

@end
