//
//  CustomButton.h
//  SofaPlay TV
//
//  Created by Gerald Voit on 11.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic, assign) BOOL shouldFocus;
@property (nonatomic, assign) BOOL isPressed;

@end
