//
//  LicenseViewController.h
//  SofaPlay TV
//
//  Created by Fabian Pimminger on 13.11.16.
//  Copyright © 2016 HighLVL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LicenseViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextView *txtView;

@end
